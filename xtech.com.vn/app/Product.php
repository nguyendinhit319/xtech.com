<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
class Product extends Model
{
     use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'product';
    protected $fillable = [
        'pr_id', 'pr_title','pr_content','pr_more_content','created_at','updated_at','pr_img'
    ];
	protected $primaryKey = 'pr_id';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	public function GetAllProducts(){
		$data = DB::table($this->table)->get();
		return $data;	
	}
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
