@extends('admin.master_admin')
@section('content')
		
			<div class = "col-lg-12">
			
				<h1 class="page-header">Khách hàng
							<small>Thêm</small>
				</h1>
					
			</div>
		
				<div class="col-12" style="padding-bottom:120px">
					@include('admin.block.error')
					<form id = "validate_form" action="{{route('admin.customer.postAdd')}}" method="POST" enctype="multipart/form-data" >
							<input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
							
									<div class="form-group">
										<label>Tên khách hàng</label>
										<input class="form-control input" id = "name" type = "text" name = "name" required ></input>
									</div>
						
									<div class="form-group">
										<label>Năm sinh</label>
										<input class="form-control input" id ="birthyear" type = "number" name = "birthyear" required ></input>
									</div>	
						
									<div class="form-group">
										<label>Địa chỉ</label>
										<input class="form-control input" id = "address" type = "text" name = "address" required ></input>
									</div>	
									
									<div class="form-group">
										<label>Email</label>
										<input class="form-control input" id = "email" type = "email" name = "email" required ></input>
									</div>	
									 
									<div class="form-group">
										<label style="display: inherit;">Ảnh đại diện</label>
										<img id="avar" class="mx-auto d-block" width="30%" height="30%" alt="avatar" src="#"/>
										<input style = "margin-top:5px" type= "file" name = "avatar" required onchange="readURL(this);"></input>
										<script>
											function readURL(input) {
													if (input.files && input.files[0]) {
														var reader = new FileReader();

														reader.onload = function (e) {
															$('#avar')
																.attr('src', e.target.result)
																.width(150)
																.height(150)
														};

														reader.readAsDataURL(input.files[0]);
													}
											}
										</script>
									</div>	
									
									
									<div class="form-group">
										<label>Thông tin khách hàng</label>
										<textarea name = "data_cus" class="form-control" rows = "3"></textarea>
										<script>
											ckeditor("data_cus");
										</script>
									</div>	
									
									
									
									<div class="form-group">
										<label>Loại khách hàng</label>
										<label class="radio-inline">
											<input type = "radio" name = "type" checked="checked" value="1">Doanh nghiệp</input>
										</label>
										<label class="radio-inline">
											<input type = "radio" name = "type" value="0">Cá nhân</input>
										</label>
								
									</div>			
									
										<button type = "submit" id = "submit" class="btn btn-default" style="background-color:#b4f1ee" onclick = "save_function()">Lưu</button>
										<button type="reset" class="btn btn-default " onclick = "reset_function()" style="margin-left: 28px;background-color:#b4f1ee">Reset</button>
										
					</form>
				
			</div>
		<script>
			function reset_function(){
				
				var getElementForm = document.getElementById('form_add');
				getElementForm.reset();
			}
			
			
		</script>
		

@endsection()

