<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
class Customer extends Model
{
     use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'custommer';
    protected $fillable = [
        'cus_id', 'cus_name', 'pr_id','cus_address','cus_type','created_at','updated_at','cus_email','cus_birthday','cus_img','cus_thongtin'
    ];
	protected $primaryKey = 'cus_id';
  
	public function GetAllCustomers(){
		$data = DB::table($this->table)->get();
		return $data;
	}
	public function getAllRecords(){
		$total_record = DB::table($this->table)
		->select('count(custommer.cus_id)')
		->get();
		return $total_record;
	}
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
