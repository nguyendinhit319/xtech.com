<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Xtech</title>
        <!-- Styles -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	   <style>
            html, .container,body {
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
            }
            .full-height {
                height: 100vh;
            }
			.link-list{
                display: inline-block;
                padding: 0px 20px;	
                line-height: 55px;
                height: 100%;
                text-align: center;
                width: auto;
                color:#ffffff;
				text-decoration: none !important;
            }
            .link-list:hover{
                background-color: #476c96;
				color:red;
            }
			.link-list{
				transition:background-color .5s;
			}
			.visible-list .vs-list-link{
				text-decoration:none;
				display:block;
				width:100%;
				padding: 3px;
				color: white;
				font-weight: normal;
			}
			.visible-list .vs-list-link:hover{
				background-color:#476c96;
			}
			.visible-list .vs-list-link{
				transition:background-color .2s;
			}
			.visible-list{
				width: 120px;
				font-size: 10px;
				margin: 0px !important;
				position: absolute;
				right: 0px;
				background-color: #1e2833;
				top: 54px;
				color:white;
			}
			#header-input-search{
				padding-top: 0.75rem;
				padding-bottom: 0.75rem;
				padding-left: 1.5rem;
				padding-right: 1.5rem;
				background-color: #f1f3f5;
				position: absolute;
				top:8px;
			}
			
			
			
        </style>
    </head>
    <body>
		<div class="container" style="width:100% ;padding:0 !important">
			<!--Navigation-->
			<div class="nav col-md-12 " style="height:55px;background-color: #1e2833;">

                <div style="height:100%;float:left" class="col-md-7 hidden-xs hidden-sm">

                    <a style ="display:inline-block;position: relative;height: 100%;width: 55px; float: left;margin-right:10px;" href="#"><img class="mx-auto d-block" src="/uploads/customers/20190419040451.PNG" width="50%" height="50%" style="width: 55px;display:block;top: 13px;position: absolute;"></a>
                    <a class = "link-list" href="#" title="Trang chủ"><i class="fas fa-home" style="font-size: 18px;"></i></a>
                    <a class = "link-list" href="#" title="Sản phẩm">Sản phẩm</a>
                    <a class = "link-list" href="#" title="Khách hàng">Khách hàng</a>
                    <a class = "link-list" href="#" title="Tin tức">Tin tức</a>
                    <a class = "link-list" href="#" title="Thiết bị">Thiết bị</a>
                    <a class = "link-list" href="#" title="Thông tin về công ty Xtech">Về chúng tôi</a>

                </div>
				<div class="search col-md-2 hidden-xs hidden-sm" style="padding-left:0;padding-right:0;position:relative">
					<input id = "header-input-search" type="search" placeholder="Search...">
						<i style="color: #fff;position: absolute;
							top: 21px;right: 32px;color:#1e2833;cursor: pointer;" class="fas fa-search" onclick="search_function()">
						</i>
					</input>
				</div>
				<div style="height:100%;float:left" class="col-xs-4 col-sm-4  visible-xs visible-sm">

                    <a style ="display:inline-block;position: relative;height: 100%;width: 55px; float: left;margin-right:10px;" href="#"><img class="mx-auto d-block" src="/uploads/customers/20190419040451.PNG" width="50%" height="50%" style="width: 55px;display:block;top: 13px;position: absolute;"></a>

                </div>
				<div style="height:100%;float:right" class="col-xs-1 col-sm-1 visible-xs visible-sm">

					<a  onclick="list_function()" style = "padding:0px !important ;width: 30px;" class = "link-list" href="#"><i class="fas fa-bars" ></i></a>   
					
                </div>
				
				<div class="col-md-3 offset-md-1 hidden-sm hidden-xs" style="float:right ">
					@if (Route::has('login'))
				
							@auth
								<a href="{{ url('/home') }}">Home</a>
							@else
								<a style ="float:right" class = "link-list" href="{{ route('login') }}">Login</a>
								@if (Route::has('register'))
									<a style ="float:right" class = "link-list" href="{{ route('register') }}">Register</a>
								@endif
							@endauth
				
					@endif
				</div>		
				
            </div>
			
			<!--Show customers-->
			<div class="">
				
			</div>
		
			<!--Show products-->
			<div class="">
				
			</div>
			
			<!--Show Services-->
			<div class="">
				
			</div>
			
			<!--Show news-->
			<div class="">
				
			</div>
			
			
		
		</div>
		
         
    </body>
	<script>
		let count = 0;
		function list_function(){
			count++;
			let el = document.querySelector(".visible-list");
			if(count % 2 === 1){
				el.style.display = "block";
			}else{
				el.style.display = "none";
			}
			
		}
		function search_function(){
			
		}
	</script>
</html>
