<?php

namespace App\Http\Controllers;
use App\User;
use App\Customer;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon; 
class CustomerController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
	private $destinationPath =  '/uploads/customers/';
	public function getList(){
		$customer = new Customer;
		$dataCustomer = $customer->GetAllCustomers();
		$list = array();
		$list = $dataCustomer;
		return view('admin.customer.list',compact('list'));
	}
	//add
	public function getAdd(){
		return view('admin.customer.add');
	}
	public function postAdd(Request $request){
		$data = $request->all();
		$customer = new Customer;
		$cus_img = '/uploads/customers'.$customer->cus_img;
		if($request->hasFile('avatar')){
			$file = Input::file('avatar');
			$filename =$file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$timestamp = str_replace([' ', ':','-'], '', Carbon::now()->toDateTimeString());
			$name =$timestamp .'.'.$extension;
			$file->move(public_path($this->destinationPath), $name);
			$customer->cus_img=$this->destinationPath.$name;
		}
		$customer->cus_name = $data['name'];
		$customer->cus_address = $data['address'];
		$customer->cus_birthday = $data['birthyear'];
		$customer->cus_email = $data['email'];
		$customer->cus_type = $data['type'];   
		$customer->cus_thongtin = $data['data_cus'];
		
		$customer->save();
		return redirect()->route('admin.customer.getList')->with(['flash_level'=>'success','flash_message'=>'Thêm thành công']);
	}
	//delete
	public function getDelete($id){
		Customer::destroy($id);
		return redirect()->route('admin.customer.getList')->with(['flash_level'=>'success','flash_message'=>'Xóa thành công']);
	}
	
	//edit
	public function getEdit($id){
		$customer = DB::table('custommer')->where('custommer.cus_id',$id)->select('custommer.*')->limit(1)->get();
		if(count($customer)==0){
			return getList();
		}
		$customer=$customer[0];
		return view('admin.customer.edit',compact('customer'));
	}
	
	public function postEdit($id,Request $request){
		$data = $request->all();
		$customer = new Customer;
		$cus_img = '/uploads/customers'.$customer->cus_img;
		if($request->hasFile('avatar')){
			$file = Input::file('avatar');
			$filename =$file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$timestamp = str_replace([' ', ':','-'], '', Carbon::now()->toDateTimeString());
			$name =$timestamp .'.'.$extension;
			$file->move(public_path($this->destinationPath), $name);
		}
		$customer=DB::table('custommer') ->where('cus_id', $data['id']) ->limit(1) ->update ([
					'cus_name' => $data['name'],
					'cus_address' => $data['address'], 
					'cus_email' => $data['email'], 
					'cus_birthday' => $data['birthyear'],
					'cus_type' => $data['type'],
					'cus_img' => $this->destinationPath.$name,
				]); 
		return redirect()->route('admin.customer.getList')->with(['flash_level'=>'success','flash_message'=>'Sửa thành công']);
	}
	
	
}
