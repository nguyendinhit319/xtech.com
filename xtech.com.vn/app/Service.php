<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use DB;
class Service extends Model
{
     use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'service';
    protected $fillable = [
        'service_id', 'service_name', 'service_content','service_more_content','created_at','updated_at','service_icon'
    ];
	protected $primaryKey = 'service_id';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	public function GetAllServices($numberPerPage){ 
		$data = DB::table($this->table)->paginate($numberPerPage);
		return $data;
	}
    public function getAllRecords(){
        $total_record = DB::table($this->table)
        ->select('count(service.service_id)')
        ->get();
        return $total_record;
    }
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
