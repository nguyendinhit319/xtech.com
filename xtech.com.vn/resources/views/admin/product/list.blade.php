@extends('admin.master_admin')
@section('content')
<div class="col-lg-12">
	<h1 class="page-header">Danh sách
		<small>SẢN PHẨM</small>

		<a title="Thêm Sản Phẩm" href="{{route('admin.product.add')}}" style="float: right;color:#4ed7e4">
			<i class="fas fa-user-plus"></i>
		</a>

	</h1>

</div>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	<thead>
		<tr>

			<th style="width:5%">STT</th>
			<th style="width:20%">Tên Sản Phẩm</th>
			<th style="width:35%">Nội Dung</th>
			<th style="width:10%">Mô Tả</th>
			<th style="width:18%">Hình ảnh minh họa</th>
			<th style="width:12%">Chỉnh sửa</th>

		</tr>
	</thead>
	<tbody>
		<?php $stt = 0; ?>
		@foreach($list as $item)
		<?php $stt++; ?>
		<tr>

			<td>{!! $stt !!}</td>
			<td>{!! $item->pr_title !!}</td>
			<td>{!! $item->pr_content !!}</td>
			<td>{!! $item->pr_more_content !!}</td>
			<td>
				<div style="text-align: center;">
					<img class="mx-auto d-block" src="{{asset($item->pr_img)}}" width="30%" height="30%" />
				</div>
			</td>

			<td>
				<a href="{!! URL::route('admin.product.edit', $item->pr_id) !!}" title="Sửa thông tin Sản Phẩm" style="text-decoration: none !important;color:#5aaf24">
					<i class="fas fa-user-edit"></i>
				</a>
				&nbsp;&nbsp;
				<a id="deleteItem" href="{!! URL::route('admin.product.delete', $item->pr_id) !!}" title="Xóa khách hàng" style="text-decoration: none !important;color:#f91b1b" onclick="return alert_function('Bạn có chắc chắn muốn xóa!')">
					<i class="fas fa-trash-alt"></i>
				</a>
			</td>


		</tr>
		@endforeach
	</tbody>
</table>

@endsection()