<?php

namespace App\Http\Controllers;

use App\User;
use App\Product;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class ProductController extends Controller
{
	private $destinationPath =  '/uploads/product/';
	public function getList()
	{
		$product = new Product;
		$dataProducts = $product->GetAllProducts();
		$list = array();
		$list = $dataProducts;

		return view('admin.product.list', compact('list'));
	}
	//add
	public function getAdd()
	{
		return view('admin.product.add');
	}
	public function postAdd(Request $request)
	{
		$data = $request->all();
		$product = new Product;
		$product->pr_title = $data['title'];
		$product->pr_content = $data['content'];
		$product->pr_more_content = $data['more_content'];
		$pr_img = '/uploads/product' . $product->pr_img;
		if ($request->hasFile('image')) {
			$file = Input::file('image');
			$filename = $file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
			$name = $timestamp . '.' . $extension;
			$file->move(public_path($this->destinationPath), $name);
			$product->pr_img = $this->destinationPath . $name;
		}
		$product->save();
		return redirect()->route('admin.product.getList')->with(['flash_level' => 'success', 'flash_message' => 'Thêm thành công']);
	}
	//delete
	public function getDelete($id)
	{
		Product::destroy($id);
		return redirect()->route('admin.product.getList')->with(['flash_level' => 'success', 'flash_message' => 'Xóa thành công']);
	}
	//edit
	public function getEdit($id)
	{
		$product = DB::table('product')->where('product.pr_id', $id)->select('product.*')->limit(1)->get();
		if (count($product) == 0) {
			return getList();
		}
		$product = $product[0];
		return view('admin.product.edit', compact('product'));
	}

	public function postEdit($id, Request $request)
	{
		$data = $request->all();
		if ($request->hasFile('image')) {
			$file = Input::file('image');
			$filename = $file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$timestamp = str_replace([' ', ':', '-'], '', Carbon::now()->toDateTimeString());
			$name = $timestamp . '.' . $extension;
			$file->move(public_path($this->destinationPath), $name);
		}
		$customer = DB::table('product')->where('cus_id', $data['id'])->limit(1)->update([
			'pr_title' => $data['title'],
			'pr_content' => $data['content'],
			'pr_more_content' => $data['more_content'],
			'cus_img' => $this->destinationPath . $name,
		]);
		return redirect()->route('admin.product.getList')->with(['flash_level' => 'success', 'flash_message' => 'Sửa thành công']);
	}
}
