@extends('admin.master_admin')
@section('content')
<div class="col-lg-12">
	<h1 class="page-header">SẢN PHẨM
		<small>Thêm</small>
	</h1>
</div>
<div class="col-lg-7" style="padding-bottom:120px">
	@include('admin.block.error')
	<form id="validate_form" action="{{route('admin.product.postAdd')}}" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<label>Tên Sản Phẩm</label>
			<input class="form-control input" id="title" type="text" name="title" required>
		</div>
		<div class="form-group">
			<label>Nội Dung</label>
			<input class="form-control input" id="content" type="text" name="content" required>
		</div>
		<div class="form-group">
			<label>Mô tả</label>
			<input class="form-control input" id="more_content" type="text" name="more_content" required>
		</div>
		<div class="form-group">
			<label>Hình ảnh minh họa</label>
			<input class="form-control input" id="image" type="file" name="image" required>
		</div>
		<button type="submit" class="btn btn-default" style="background-color:#b4f1ee">Lưu</button>
		<button type="reset" class="btn btn-default " onclick="reset_function()" style="margin-left: 28px;background-color:#b4f1ee">Reset</button>
	</form>
</div>
<script>
	function reset_function() {
		var getElementForm = document.getElementById('form_add');
		getElementForm.reset();
	}
</script>
@endsection()