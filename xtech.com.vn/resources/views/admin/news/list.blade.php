@extends('admin.master_admin')
@section('content')
	<div class="col-lg-12">
		<h1 class="page-header">Tin tức
			
					<a title ="Thêm khách hàng" href="{{route('admin.news.add')}}" style="float: right;color:#4ed7e4">
					<i class="fas fa-user-plus"></i>
				</a>
			
		</h1>
	</div>
	
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
			
				<th >Stt</th>
				<th >Tiêu đề</th>
				<th >Nội dung</th>
				<th >Ngày đăng</th>
				<th>Ngày cập nhật</th>
				<th >Hình ảnh</th>
				<th >Chức năng</th>
																		
			</tr>
		</thead>
		<tbody>
			<?php $stt=0;?>
			@foreach($list as $item)
			<?php $stt++;?>
			<tr>
			
				<td>{!! $stt !!}</td>
				<td>{!! $item->news_name !!}</td>
				<td>{!! $item->news_content !!}</td>
				<td>{!! $item->created_at !!}</td>
				<td>{!! $item->updated_at !!}</td>
				<td>
					<div style="text-align: center;">
						<img  class="mx-auto d-block" src="{{asset($item->news_img)}}" width="30%" height="30%"/>
					</div>	
				</td>
				<td style="text-align: center">
					<a href="{!! URL::route('admin.news.edit', $item->news_id) !!}" title="Sửa thông tin khách hàng" style="text-decoration: none !important;color:#5aaf24">
						<i class="fas fa-user-edit"></i>
					</a> 
					<a id = "deleteItem"  href="{!! URL::route('admin.news.delete', $item->news_id) !!}" title="Xóa khách hàng" style="text-decoration: none !important;color:#f91b1b" onclick="return alert_function('Bạn có chắc chắn muốn xóa!')">
						<i class="fas fa-trash-alt"></i>
						<script>
							function alert_function(msg){
								if(confirm(msg)){
									return true;
								}
								return false;
							};	
						</script>
					</a>	
				</td>

				
			</tr>
			@endforeach
		</tbody>
	</table>
			
@endsection()
