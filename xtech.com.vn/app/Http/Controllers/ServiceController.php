<?php

namespace App\Http\Controllers;
use App\User;
use App\Customer;
use App\Service;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon; 
class ServiceController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
	private $destinationPath =  '/uploads/service/';
	public function getList(){
		$service = new Service;
		$dataService = $service->GetAllServices(100);
		$list = array();
		$list = $dataService;
		return view('admin.service.service_list', compact('list'));
	}
	//add
	public function getAdd(){
		return view('admin.service.service_add');
	}
	public function postAdd(Request $request){
		$data = $request->all();
		$service = new Service;
		$service->service_name = $data['name'];
		$service->service_content = $data['content'];
		$service->service_more_content = $data['more_content'];
		$service_icon = '/uploads/service'.$service->service_icon;
		if($request->hasFile('icon')){
			$file = Input::file('icon');
			$filename =$file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$timestamp = str_replace([' ', ':','-'], '', Carbon::now()->toDateTimeString());
			$name =$timestamp .'.'.$extension;
			$file->move(public_path($this->destinationPath), $name);
			$service->service_icon=$this->destinationPath.$name;
		}
		$service->save();
		return redirect()->route('admin.service.getList')->with(['flash_level'=>'success','flash_message'=>'Thêm thành công']);
	}
	//delete
	public function getDelete($id){
		Service::destroy($id);
		return redirect()->route('admin.service.getList')->with(['flash_level'=>'success','flash_message'=>'Xóa thành công']);
	} 
	//edit
	public function getEdit($id){
		$service = DB::table('service')->where('service.service_id',$id)->select('service.*')->limit(1)->get();
		if(count($service)==0){
			return getList();
		}
		$service=$service[0];
		return view('admin.service.service_edit',compact('service'));
	}
	
	public function postEdit($id,Request $request){
		$data = $request->all();
		if($request->hasFile('icon')){
			$file = Input::file('icon');
			$filename =$file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$timestamp = str_replace([' ', ':','-'], '', Carbon::now()->toDateTimeString());
			$name =$timestamp .'.'.$extension;
			$file->move(public_path($this->destinationPath), $name);
		}
		$customer=DB::table('service') ->where('service_id', $data['id']) ->limit(1) ->update ([
					'service_name' => $data['name'],
					'service_content' => $data['content'], 
					'service_more_content' => $data['more_content'], 
					'service_icon' => $this->destinationPath.$name,
				]); 
		
		
		return redirect()->route('admin.customer.getList')->with(['flash_level'=>'success','flash_message'=>'Sửa thành công']);
	}
	
	
	
	
}
