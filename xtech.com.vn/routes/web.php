<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	
    return view('welcome');
});

//-------------------------------------------Customer------------------------------------------
Route::get('/test', 'CustomerController@index');



Route::get('xtech.com',function(){
	return "test1";
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin/customer'],function(){
	
		Route::get('/',['as'=>'admin.customer.getList','uses'=>'CustomerController@getList']);
		Route::get('list',['as'=>'admin.customer.getList','uses'=>'CustomerController@getList']);
		Route::get('add',['as'=>'admin.customer.add','uses'=>'CustomerController@getAdd']);
		Route::post('add',['as'=>'admin.customer.postAdd','uses'=>'CustomerController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.customer.delete','uses'=>'CustomerController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.customer.edit','uses'=>'CustomerController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.customer.postEdit','uses'=>'CustomerController@postEdit']);
		

});
//Tung
Route::group(['prefix'=>'admin/news'],function(){
	
		Route::get('/',['as'=>'admin.news.getList','uses'=>'NewsController@getList']);
		Route::get('list',['as'=>'admin.news.getList','uses'=>'NewsController@getList']);
		Route::get('add',['as'=>'admin.news.add','uses'=>'NewsController@getAdd']);
		Route::post('add',['as'=>'admin.news.postAdd','uses'=>'NewsController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.news.delete','uses'=>'NewsController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.news.edit','uses'=>'NewsController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.news.postEdit','uses'=>'NewsController@postEdit']);
		
	
});
//-------------------------------------------Service------------------------------------------

Route::group(['prefix'=>'admin/service'],function(){
	
		Route::get('/',['as'=>'admin.service.getList','uses'=>'ServiceController@getList']);
		Route::get('list',['as'=>'admin.service.getList','uses'=>'ServiceController@getList']);
		Route::get('add',['as'=>'admin.service.add','uses'=>'ServiceController@getAdd']);
		Route::post('add',['as'=>'admin.service.postAdd','uses'=>'ServiceController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.service.delete','uses'=>'ServiceController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.service.edit','uses'=>'ServiceController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.service.postEdit','uses'=>'ServiceController@postEdit']);
		

});
//Nguyen
Route::group(['prefix'=>'admin/product'],function(){
	
		Route::get('/',['as'=>'admin.product.getList','uses'=>'ProductController@getList']);
		Route::get('list',['as'=>'admin.product.getList','uses'=>'ProductController@getList']);
		Route::get('add',['as'=>'admin.product.add','uses'=>'ProductController@getAdd']);
		Route::post('add',['as'=>'admin.product.postAdd','uses'=>'ProductController@postAdd']);
		Route::get('delete/{id}',['as'=>'admin.product.delete','uses'=>'ProductController@getDelete']);
		Route::get('edit/{id}',['as'=>'admin.product.edit','uses'=>'ProductController@getEdit']);
		Route::post('edit/{id}',['as'=>'admin.product.postEdit','uses'=>'ProductController@postEdit']);
		
	
});