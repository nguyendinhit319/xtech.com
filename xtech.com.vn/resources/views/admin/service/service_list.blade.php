@extends('admin.master_admin')
@section('content')
	<div class="col-lg-12">
		<h1 class="page-header">Danh sách
			<small>Dịch vụ</small>
			<a title ="Thêm dịch vụ" href="{{route('admin.service.add')}}" style="float: right;">
				<i class="fas fa-plus"></i>
			</a>
		</h1>
	</div>
	
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>STT</th>
				<th>Tên dịch vụ</th>
				<th>Nội dung ngắn</th>
				<th>Nội dung </th>
				<th>Ngày tạo </th>
				<th>Ngày cập nhật</th>
				<th>Icon</th>
				<th></th>													
			</tr>
		</thead>
		<tbody>
			<?php $stt=0;?>
			@foreach($list as $item)
			<?php $stt++;?>
			<tr>
				<td>{!! $stt !!}</td>
				<td>{!! $item->service_name !!}</td>
				<td>{!! $item->service_content !!}</td>
				<td>{!! $item->service_more_content !!}</td>
				<td>{!! $item->created_at !!}</td>
				<td>{!! $item->updated_at !!}
				<td>
					<div style="text-align: center;">
						<img  class="mx-auto d-block" src="{{asset($item->service_icon)}}" width="30%" height="30%"/>
					</div>
				</td>
				<td>
					<a href="{!! URL::route('admin.service.edit', $item->service_id) !!}" title="Sửa thông tin dịch vụ" style="text-decoration: none !important;color:#5aaf24">
						<i class="far fa-edit"></i>
					</a> |
					<a href="{!! URL::route('admin.service.delete', $item->service_id) !!}" title="Xóa dịch vụ" style="text-decoration: none !important;color:#5aaf24" onclick="return alert_function('Bạn có chắc chắn muốn xóa!')">
						<i class="fas fa-trash-alt"></i>
						<script>
							function alert_function(msg){
								if(confirm(msg)){
									return true;
								}
								return false;
							};	
						</script>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
			
@endsection()
