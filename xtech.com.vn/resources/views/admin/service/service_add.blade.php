@extends('admin.master_admin')
@section('content')
		
			<div class = "col-lg-12">
			
				<h1 class="page-header">DỊCH VỤ
					<small>Thêm Dịch Vụ</small>
				</h1>
					
			</div>
			<div class="col-lg-7" style="padding-bottom:120px">
				@include('admin.block.error')
				<form id = "form_add" action="{{route('admin.service.postAdd')}}" method="POST" enctype="multipart/form-data" >
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
								<div class="form-group">
									<label>Tên Dịch Vụ</label>
									<input class="form-control" type = "text" name = "name" >
								</div>
								<div class="form-group">
									<label>Nội Dung Ngắn</label>
									<input class="form-control" type = "text" name = "content" >
								</div>
								<div class="form-group">
									<label>Nội Dung</label>
									<input class="form-control" type = "text" name = "more_content" >
								</div>	
								<div class="form-group">
									<label>Icon</label>
									<input class="form-control" type = "file" name = "icon" >
								</div>	
									<button type = "submit" class="btn btn-default" style="background-color:#b4f1ee">Save</button>
									<button type="reset" class="btn btn-default " onclick = "reset_function()" style="margin-left: 28px;background-color:#b4f1ee">Reset</button>
				</form>
			</div>
		
		<script>
			function reset_function(){
				
				var getElementForm = document.getElementById('form_add');
				getElementForm.reset();
			}
			
		</script>
		

@endsection()

