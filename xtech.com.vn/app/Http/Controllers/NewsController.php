<?php

namespace App\Http\Controllers;
use App\User;
use App\News;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon; 
class NewsController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return View
     */
	private $destinationPath =  '/uploads/news/';
	public function getList(){
		$news = new News;
		$datanews = $news->GetAllNews(100);
		$list = array();
		$list = $datanews;
		return view('admin.news.list', compact('list'));
	}
	//add
	public function getAdd(){
		return view('admin.news.add');
	}
	public function postAdd(Request $request){
		$data = $request->all();
		$news = new News;
		$news->news_name = $data['name'];
		$news->news_content = $data['content'];
	
		$news_img = '/uploads/news'.$news->news_img;
		if($request->hasFile('image')){
			$file = Input::file('image');
			$filename =$file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$timestamp = str_replace([' ', ':','-'], '', Carbon::now()->toDateTimeString());
			$name =$timestamp .'.'.$extension;
			$file->move(public_path($this->destinationPath), $name);
			$news->news_img=$this->destinationPath.$name;
		}
		$news->save();
		return redirect()->route('admin.news.getList')->with(['flash_level'=>'success','flash_message'=>'Thêm thành công']);
	}
	//delete
	public function getDelete($id){
		News::destroy($id);
		return redirect()->route('admin.news.getList')->with(['flash_level'=>'success','flash_message'=>'Xóa thành công']);
	} 
	//edit
	public function getEdit($id){
		$news = DB::table('news')->where('news.news_id',$id)->select('news.*')->limit(1)->get();
		if(count($news)==0){
			return getList();
		}
		$news=$news[0];
		return view('admin.news.edit',compact('news'));
	}
	
	
	public function postEdit($id,Request $request){
		$data = $request->all();
		$news = new News;
		$cus_img = '/uploads/news'.$news->news_img;
		if($request->hasFile('image')){
			$file = Input::file('image');
			$filename =$file->getClientOriginalName();
			$extension = $file->getClientOriginalExtension();
			$timestamp = str_replace([' ', ':','-'], '', Carbon::now()->toDateTimeString());
			$name =$timestamp .'.'.$extension;
			$file->move(public_path($this->destinationPath), $name);
		}
		$news=DB::table('news') ->where('news_id', $data['id']) ->limit(1) ->update ([
					'news_name' => $data['name'],
					'news_content' => $data['content'], 
					'news_img' => $this->destinationPath.$name,
				]); 
		return redirect()->route('admin.news.getList')->with(['flash_level'=>'success','flash_message'=>'Sửa thành công']);
	}
	
	
	
	
}
