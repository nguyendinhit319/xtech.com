-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 11, 2019 lúc 09:48 AM
-- Phiên bản máy phục vụ: 10.1.37-MariaDB
-- Phiên bản PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `xtech`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `custommer`
--

CREATE TABLE `custommer` (
  `cus_name` varchar(255) DEFAULT NULL,
  `cus_id` int(11) NOT NULL,
  `cus_address` varchar(255) NOT NULL,
  `cus_type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cus_email` varchar(255) NOT NULL,
  `cus_birthday` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `custommer`
--

INSERT INTO `custommer` (`cus_name`, `cus_id`, `cus_address`, `cus_type`, `created_at`, `updated_at`, `cus_email`, `cus_birthday`) VALUES
('Minh Hạnh', 1, 'Việt Nam', 1, '2019-04-10 08:50:04', '2019-04-10 08:50:04', 'hanh@gmail.com', '2003'),
('Minh Hạnh', 2, 'Việt Nam', 0, '2019-04-10 10:19:13', '2019-04-10 10:19:13', 'hanh@gmail.com', '1999'),
('Minh Hạnh', 3, 'Việt Nam', 1, '2019-04-10 10:28:22', '2019-04-10 10:28:22', 'hanh@gmail.com', '1999'),
('Minh Hạnh', 4, 'Viet Nam', 1, '2019-04-10 10:29:11', '2019-04-10 10:29:11', 'hanh@gmail.com', '1999'),
('Minh Hạnh', 5, 'Việt Nam', 1, '2019-04-10 10:29:26', '2019-04-10 10:29:26', 'hanh@gmail.com', '1999'),
('DemoRepo', 6, 'Việt Nam', 1, '2019-04-10 10:29:41', '2019-04-10 10:29:41', 'ngoc@gmail.com', '1999'),
('DemoRepo', 7, 'Viet Nam', 1, '2019-04-10 10:29:53', '2019-04-10 10:29:53', 'minhhanh@gmail.com', '1999'),
('Minh Hạnh', 8, 'Việt Nam', 1, '2019-04-10 10:30:35', '2019-04-10 10:30:35', 'hanh@gmail.com', '1999'),
('van tu', 9, 'Việt Nam', 1, '2019-04-10 10:31:36', '2019-04-10 10:31:36', 'vantund2017@gmail.com', '1999'),
('Minh Hạnh', 10, 'Viet Nam', 1, '2019-04-10 10:31:45', '2019-04-10 10:31:45', 'hanh@gmail.com', '1999'),
('Minh Hạnh', 11, 'Viet Nam', 1, '2019-04-10 10:31:55', '2019-04-10 10:31:55', 'hanh@gmail.com', '1999');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_03_25_081724_create_users_table', 1),
(2, '2019_03_25_081759_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `news_name` varchar(255) DEFAULT NULL,
  `news_content` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `news_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`news_id`, `news_name`, `news_content`, `created_at`, `updated_at`, `news_img`) VALUES
(1, 'Article Title', 'Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.', '2019-03-25 08:11:47', '2019-03-25 08:11:47', NULL),
(2, 'Article Title', 'Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.', '2019-03-25 08:11:47', '2019-03-25 08:11:47', NULL),
(3, 'Article Title', 'Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est, interdum justo suscipit id.', '2019-03-25 08:11:47', '2019-03-25 08:11:47', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `pr_id` int(11) NOT NULL,
  `pr_title` varchar(255) DEFAULT NULL,
  `pr_content` varchar(255) DEFAULT NULL,
  `pr_more_content` mediumtext,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `pr_img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`pr_id`, `pr_title`, `pr_content`, `pr_more_content`, `created_at`, `updated_at`, `pr_img`) VALUES
(1, 'XMAGIC', 'Ứng dụng kết nối người cung cấp và người sử dụng dịch vụ', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL),
(2, 'ELAB', 'Ứng dụng Khám phá cuộc sống quanh bé.\r\n', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL),
(3, 'ISS', 'Giải pháp Đào tạo mô phỏng công nghiệp.\r\n', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL),
(4, 'OTM', 'Giải pháp \r\nGiảng dạy trực tuyến.', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL),
(5, 'DMS', 'Giải pháp\r\nQuản lý tài liệu, dự án.', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL),
(6, 'OCR', 'Nhận dạng và số hóa văn bản.', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL),
(7, 'ECO HOME', 'Giải pháp\r\nNhà thông minh.', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL),
(8, 'ECOLIGHT', 'Giải pháp\r\nChiếu sáng thông minh.', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL),
(9, 'SEMART', 'Hệ thống Thiết bị Điện - Điện tử thông minh.\r\n', NULL, '2019-03-25 08:05:21', '2019-03-25 08:05:21', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `service`
--

CREATE TABLE `service` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(255) DEFAULT NULL,
  `service_content` varchar(255) DEFAULT NULL,
  `service_more_content` mediumtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `service_icon` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `service`
--

INSERT INTO `service` (`service_id`, `service_name`, `service_content`, `service_more_content`, `created_at`, `updated_at`, `service_icon`) VALUES
(1, 'Tích hợp hệ thống', 'Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.', NULL, '2019-03-25 08:07:17', '2019-03-25 08:07:17', ''),
(2, 'Chuyển giao công nghệ phần mềm', 'Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.', NULL, '2019-03-25 08:07:17', '2019-03-25 08:07:17', ''),
(3, 'Bảo trì, bảo hành, hoạt động của phần mềm và HTTT', 'Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.', NULL, '2019-03-25 08:07:17', '2019-03-25 08:07:17', ''),
(4, 'Tư vấn hệ thống mạng và quản trị hệ thống máy vi tính', 'Aenean tortor est, vulputate quis leo in, vehicula rhoncus lacus. Praesent aliquam in tellus eu gravida. Aliquam varius finibus est.', NULL, '2019-03-25 08:07:17', '2019-03-25 08:07:17', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'van tu', 'vantund2017@gmail.com', NULL, '$2y$10$z9S8TtCn8vo1axcG17XWluloRFm4oDPYQYgCYjBairrCq8SBaNu3a', 'vQZ6OigKLAaq3zY6k9y127l9VmCFNYybE7auaACOrS72Mqlpic1oSUjCBYNR', '2019-03-28 01:30:42', '2019-03-28 01:30:42'),
(2, 'tung nguyen', 'tung@gmail.com', NULL, '$2y$10$TnRyyNkXSHl23anyBCM2C.vmlufkbceh873OZsisdq.2K3YoIw.pe', NULL, '2019-03-28 01:45:38', '2019-03-28 01:45:38');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `custommer`
--
ALTER TABLE `custommer`
  ADD PRIMARY KEY (`cus_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`pr_id`);

--
-- Chỉ mục cho bảng `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`service_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `custommer`
--
ALTER TABLE `custommer`
  MODIFY `cus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `pr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `service`
--
ALTER TABLE `service`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
