@extends('admin.master_admin')
@section('content')
<div class="col-lg-12">
	<h1 class="page-header">Danh sách
		<small>khách hàng</small>

		<a title="Thêm khách hàng" href="{{route('admin.customer.add')}}" style="float: right;color:#4ed7e4">
			<i class="fas fa-user-plus"></i>
		</a>

	</h1>

</div>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	<thead>
		<tr>

			<th style="background-color:#50bbc5; width: 8%;">STT</th>
			<th style="background-color:#50bbc5; width: 15%">Ảnh đại diện</th>
			<th style="background-color:#50bbc5; width: 15%">Tên khách hàng</th>
			<th style="background-color:#50bbc5; width: 11%">Ngày sinh</th>
			<th style="background-color:#50bbc5; width: 10%">Địa chỉ</th>
			<th style="background-color:#50bbc5">Email</th>
			<th style="background-color:#50bbc5">Ngày bán</th>
			<th style="background-color:#50bbc5">Ngày cập nhật</th>
			<th style="background-color:#50bbc5">Chức năng</th>
		</tr>
	</thead>
	<tbody>
		<?php $stt = 0; ?>
		@foreach($list as $item)
		<?php $stt++; ?>

		<tr>
			<td>{!! $stt !!}</td>
			<td>
				<div style="text-align: center;">
					<img class="mx-auto d-block" src="{{asset($item->cus_img)}}" width="30%" height="30%" />
				</div>
			</td>
			<td>{!! $item->cus_name !!}</td>
			<td>{!! $item->cus_birthday !!}</td>
			<td>{!! $item->cus_address !!}</td>
			<td>{!! $item->cus_email !!}</td>
			<td>{!! $item->created_at !!}</td>
			<td>{!! $item->updated_at !!}</td>
			<td style="text-align: center">
				<a href="{!! URL::route('admin.customer.edit', $item->cus_id) !!}" title="Sửa thông tin khách hàng" style="text-decoration: none !important;color:#5aaf24">
					<i class="fas fa-user-edit"></i>
				</a>
				<a id="deleteItem" href="{!! URL::route('admin.customer.delete', $item->cus_id) !!}" title="Xóa khách hàng" style="text-decoration: none !important;color:#f91b1b" onclick="return alert_function('Bạn có chắc chắn muốn xóa!')">
					<i class="fas fa-trash-alt"></i>
					<script>
						function alert_function(msg) {
							if (confirm(msg)) {
								return true;
							}
							return false;
						};
					</script>
				</a>
			</td>
		</tr>
		@endforeach
	</tbody>

</table>

@endsection()