@extends('admin.master_admin')
@section('content')
	
			<div class = "col-lg-12">
					<h1 class="page-header">Khách hàng 
						<small>Sửa</small>
					</h1>
			</div>
			<div class="col-12" style="padding-bottom:120px">
					@include('admin.block.error')
					<form id = "form_add" action="{!! URL::route('admin.customer.edit', $customer->cus_id) !!}" method="POST" enctype="multipart/form-data" >
						
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type = "hidden" name = "id" autofocus="autofocus" required="required" value="{!! $customer->cus_id !!}" ></input>
						
						<div class="form-group">		
							<label>Tên khách hàng</label>
							<input  class = "form-control input" id = "name" type = "text" name = "name" autofocus="autofocus" required="required" value="{!! $customer->cus_name !!}"></input>
						</div>				
					
						<div class="form-group">
							<label>Năm sinh</label>
							<input class = "form-control input"  id = "birthyear" type = "number" name = "birthyear" autofocus="autofocus" required="required" value="{!! $customer->cus_birthday !!}"></input>
						</div>	
							
						<div class="form-group">
							<label>Địa chỉ</label>
							<input class = "form-control input" id = "address" type = "text" name = "address" autofocus="autofocus" required="required" value="{!! $customer->cus_address !!}"></input>
						</div>	
						
						<div class="form-group">
							<label>Email</label>
							<input class = "form-control input" id = "email" type = "email" name = "email" autofocus="autofocus" required="required" value="{!! $customer->cus_email !!}"></input>
						</div>	
						
						<div class="form-group">
							<label style="display: inherit;">Ảnh đại diện</label>
							<img id = "avar" class="mx-auto d-block" width="30%" height="30%" alt="avatar" src="{{asset($customer->cus_img)}}"/>
							<input style="margin-top: 5px;" type = "file" name = "avatar" required onchange="readURL(this);"></input>
							<script>
											function readURL(input) {
													if (input.files && input.files[0]) {
														var reader = new FileReader();
														reader.onload = function (e) {
															$('#avar')
																.attr('src', e.target.result)
																.width(150)
																.height(150);
														};

														reader.readAsDataURL(input.files[0]);
													}
											}
							</script>
						</div>	
						
						<div class="form-group">
							<label>Thông tin khách hàng</label>
							<textarea name = "data_cus" class="form-control" rows = "3" >{!! $customer->cus_thongtin !!}</textarea>
							<script>
								ckeditor("data_cus");
							</script>
						</div>	
						
						<div class="form-group">
							<label >Loại khách hàng</label>
							<label class="radio-inline">
								<input type = "radio" name = "type" @if("$customer->cus_type"==1) checked="checked" @endif value="1">Doanh nghiệp</input>
							</label>
							<label class="radio-inline">
								<input type = "radio" name = "type" @if("$customer->cus_type"==0) checked="checked" @endif value="0">Cá nhân</input>
							</label>
						</div>
						
						<button type = "submit" class="btn btn-default " onclick = "save_function()" style="background-color:#b4f1ee">Lưu</button>
						<button type="reset" class="btn btn-default " onclick = "reset_function()" style="margin-left: 28px;background-color:#b4f1ee">Reset</button>
						
					</form>
		</div>
	
		<script>
			function reset_function(){
				
				var getElementForm = document.getElementById('form_add');
				getElementForm.reset();
			}
			
		</script>
	</div>
@endsection()
