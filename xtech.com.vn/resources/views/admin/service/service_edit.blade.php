@extends('admin.master_admin')
@section('content')
	
			<div class = "col-lg-12">
					<h1 class="header-page">Dịch Vụ
						<small>Sửa</small>
					</h1>
			</div>
			<div class="col-lg-7">
					<form id = "form_add" action="{!! URL::route('admin.service.edit', $service->service_id) !!}" method="POST" enctype="multipart/form-data" >
						
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type = "hidden" name = "id" autofocus="autofocus" required="required" value="{!! $service->service_id !!}" >
						<div class="form-group">		
							<label>Tên Dịch Vụ</label>
							<input  class = "form-control" type = "text" name = "name" autofocus="autofocus" required="required" value="{!! $service->service_name !!}">
						</div>				
					
						<div class="form-group">
							<label>Nội Dung Ngắn</label>
							<input class = "form-control" type = "text" name = "content" autofocus="autofocus" required="required" value="{!! $service->service_content !!}">
						</div>	
							
						<div class="form-group">
							<label>Nội dung</label>
							<input class = "form-control" type = "text" name = "more_content" autofocus="autofocus" required="required" value="{!! $service->service_more_content !!}">
						</div>	
						<div class="form-group">
							<label>Icon</label>
							<input class = "form-control" type = "file" name = "icon" autofocus="autofocus" required="required" value="{!! $service->service_icon !!}">
						</div>	
						
						<button type = "submit" class="btn btn-default btn_action" onclick="save_function()" style="background-color:#b4f1ee">Save</button>
						<button type="reset" class="btn btn-default btn_action" onclick = "reset_function()" style="margin-left: 28px;background-color:#b4f1ee">Reset</button>
						
					</form>
		</div>
	
		<script>
			function reset_function(){
				
				var getElementForm = document.getElementById('form_add');
				getElementForm.reset();
			}
			function save_function(){
				
			}
			
		</script>
	</div>
@endsection()
